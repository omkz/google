//https://www.googleapis.com/plus/v1/people/111505099809820152561/

var http    = require('blaast/simple-http');
var QS = require('querystring');

var param = {plot: 'short', r: 'json', t: 'Transformer', tomatoes:'true'};
var url = "http://www.imdbapi.com/?" + QS.stringify(param);
console.log('url : ' + url);
http.get(url, {
    ok: function(data) {
        console.log(data);
        //parsing to JSON
        data = JSON.parse(data);
    },
    error: function(err) {
        console.log('err: ' + err);
    }
});